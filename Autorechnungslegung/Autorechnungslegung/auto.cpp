
#include "auto.h"


Auto::Auto(const string marke, const string typ, const int preis, bauteil** parr_bt) :
_marke(marke), _typ(typ), _preis(preis)
{
	_bauelem = new bauteil *[MAX_BAUELEM];
	_bauelem[0] = new motor((motor *)parr_bt[0]);
	_bauelem[1] = new gangschaltung((gangschaltung *)parr_bt[1]);
}

Auto::Auto(const Auto &theO) : _marke(theO._marke), _typ(theO._typ), _preis(theO._preis)
{
	_bauelem = new bauteil *[MAX_BAUELEM];
	_bauelem[0] = new motor((motor *)theO._bauelem[0]);
	_bauelem[1] = new gangschaltung((gangschaltung *)theO._bauelem[1]);
}

Auto::~Auto()
{
	delete _bauelem[0];	_bauelem[0] = nullptr;
	delete _bauelem[1];	_bauelem[1] = nullptr;
	delete[] _bauelem;	_bauelem = nullptr;
}


void Auto::print() const
{
	cout << "\n\tMarke:\t" << _marke << "\n\tTyp:\t" << _typ << "\n\tPreis:\t" << _preis;
	_bauelem[0]->print();
	_bauelem[1]->print();
	cout << "\n\t- - - - - - - - - - - o - - - - - - - - - - -" << endl;
	return;
}
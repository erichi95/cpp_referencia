
#ifndef AUTOHAEND_H
#define AUTOHAEND_H

#include <iostream>
#include <string>

#include "auto.h"

using namespace std;

//! Die Klasse "Autohaendler"
/*!
	Diese Klasse enth�lt den Namen, den Ort des Autoh�ndlers und den Sortiment der Autos.
	Die Anzahl der Autos im Sortiment wird auch gespeichert.
	Hier findet man die entsprechende Konstruktoren, die Destruktor, eine print() Funktion,
	die "+=" Operatoren und die getter() und setter() Funktionen.
*/
class Autohaendler
{
private:
	string _name;		//!< Der Name des Autoh�ndlers.
	string _ort;		//!< Der Ort des Autoh�ndlers.

	int _anz;			//!< Die Anzahl der Elemente im Sortiment.
	Auto **_sortiment;	//!< Pointer-array von Autos.

public:
	//! Konstruktor
	/*!
		Der "_sortiment" Pointer wird auf Nullpointer gesetzt.
		Die Aufnahme der Autos wird sp�ter, mit dem "+=" Operator m�glich.
	*/
	Autohaendler(const string name = "", const string ort = "") : _name(name), _ort(ort)
	{
		_anz = 0;  _sortiment = nullptr;
	}

	//! Destruktor
	/*!
		Die Autos und Ponter-array werden gel�scht, und auf Nullpointer gesetzt.
		Die "_anz" (Anzahl der Autos im Sortiment) wird 0.
	*/
	~Autohaendler();

	//! Die print() Funktion.
	/*!
		Schreibt die Daten des Autoh�ndlers und der Autos aus.
		Ruft die print() Funktion jedes im Sortiment gefundenen Auto.
	*/
	void print() const;

	//! Die "+=" Operatoren.
	/*!
		Es gibt zwei solche Operatoren.
		Der Erste f�gt nur ein Auto zur Sortiment hinzu,
		der zweite nimmt einen ganzen Autoh�ndler �ber.
		Im zweiter Fall wird der Sortiment der �bergenommene Autoh�ndler gel�scht,
		und auf Nullpointer gesetzt.
	*/
	void operator+=(Auto &);
	void operator+=(Autohaendler &);

	//! Die getter() und setter() Funktionen.
	void setName(const string name)	{ _name = name; }
	string getName()				{ return _name; }
	void setOrt(const string ort)	{ _ort = ort; }
	string getOrt()					{ return _ort; }
};

#endif	//	AUTOHAEND_H

#ifndef BAUTEIL_H
#define BAUTEIL_H

#include <iostream>
#include <string>

using namespace std;


//!	Die Klasse "bauteil" ist eine Basisklasse.
/*!
	Von dieser Klasse werden die Klassen "motor" und "gangschaltung" vererbt.
	Die enth�lt zwei Variablen; eine string "_hersteller" und eine int "_preis",
	die dazu geh�rige Konstruktoren, eine virtuelle Destruktor, eine rein virtuelle print()
	Funktion und die getter(), setter() Funktionen.
*/
class bauteil
{
private:
	string _hersteller;	//!< Der Hersteller des Bauteils (string).
	int _preis;			//!< Der Preis des Bauteils (integer).

public:
	//! Konstruktor...
	bauteil(const string herst = "", const int preis = 0) : _hersteller(herst), _preis(preis) {}

	//! Kopiekonstruktor NACH POINTER.
	/*!
		Kopiert nach Pointer, weil mit homogener Kollektion das ben�tigt ist.
	*/
	bauteil(const bauteil *);

	//! Leere, virtuelle Destruktor, die Klasse hat keine dynamische Variable.
	virtual ~bauteil() {}

	//! Rein virtuelle print() Funktion.
	virtual void print() const = 0;

	//! Setter() und getter() Funktionen.
	/*!
		Die setter() Funktionen haben keinen R�ckgabewert, sie stellen die entsprechende Variablen ein.
		Die getter() Funktionen geben die entsprechende Variablen zur�ck.
	*/
	virtual void setHerst(const string herst)	{ _hersteller = herst; }
	virtual string getHerst()					{ return _hersteller; }
	virtual void setPreis(const int preis)		{ _preis = preis; }
	virtual int getPreis()						{ return _preis; }
};


//!	Klasse "motor", von "bauteil" vererbt.
class motor : public bauteil
{
private:
	int _pferdekraft;	//!< Pferdekraft des Motors (integer).
	int _drehmoment;	//!< Drehmoment des Motors (integer).
	int _hubraum;		//!< Hubraum des Motors (integer).

public:
	//! Public enum Variable "_kstoff", entscheidet welche Kraftstoff der Motor benutzt.
	enum Kraftstoff {
		BENZIN,	//!< Der Motor wirkt mit Benzin.
		DIESEL	//!< Der Motor wirkt mit Diesel.
	};
	enum Kraftstoff _kstoff;

	//! Konstruktor f�r "motor", ruft die Konstruktor von "bauteil".
	motor(const string herst, const int preis, const int pferdk,
		const int dreh, const int hubraum, const Kraftstoff kst) :
		bauteil(herst, preis), _pferdekraft(pferdk), _drehmoment(dreh),
		_hubraum(hubraum), _kstoff(kst) {}

	//! Kopiekonstruktor NACH POINTER, ruft die Kopiekonstruktor von "bauteil".
	motor(const motor *);

	//! Leere Destruktor.
	virtual ~motor() {}

	//! Die print() Funktion von "motor", ruft die print() Funktion von "bauteil".
	virtual void print() const;

	//! Setter() und getter() Funktionen.
	/*!
		Die setter() und getter() Funktionen, die den Hersteller und den Preis
		einstellen/abfragen rufen die entsprechende Funktionen in "bauteil".
	*/
	void setHerst(const string herst)	{ bauteil::setHerst(herst); }
	string getHerst()					{ return bauteil::getHerst(); }
	void setPreis(const int preis)		{ bauteil::setPreis(preis); }
	int getPreis()						{ return bauteil::getPreis(); }
	void setPferdk(const int pferdk)	{ _pferdekraft = pferdk; }
	int getPferd()						{ return _pferdekraft; }
	void setDreh(const int dreh)		{ _drehmoment = dreh; }
	int getDreh()						{ return _drehmoment; }
	void setHubraum(const int hubraum)	{ _hubraum = hubraum; }
	int getHubraum()					{ return _hubraum; }

	void setMotorBENZIN()				{ _kstoff = BENZIN; }
	void setMotorDIESEL()				{ _kstoff = DIESEL; }
};


//! Klasse "gangschaltung", von "bauteil" vererbt.
class gangschaltung : public bauteil
{
private:
	int _gangstellung;	//!< Anzahl der Gangstellungen (integer).

public:
	//! Public enum Variable "_typ", entscheidet wie die Gangschaltung funktioniert.
	enum Typ {
		MANUELL,		//!< Gangschaltung wird manuell.
		AUTOMATISCH,	//!< Gangschaltung wird automatisch.
		SEQUENTIELL		//!< Gangschaltung wird sequentiell.
	};
	enum Typ _typ;

	//! Konstruktor, ruft die Konstruktor von "bauteil".
	gangschaltung(const string herst, const int preis, const int gangsch, const Typ typ) :
		bauteil(herst, preis), _gangstellung(gangsch), _typ(typ) {}

	//! Kopiekonstruktor NACH POINTER, ruft die Kopiekonstruktor von "bauteil".
	gangschaltung(const gangschaltung *);

	//! Leere Destruktor.
	virtual ~gangschaltung() {}

	//! Die print() Funktion, ruft die print() Funktion von "bauteil".
	virtual void print() const;

	//! Setter() und getter() Funktionen.
	/*!
	Die setter() und getter() Funktionen, die den Hersteller und den Preis
	einstellen/abfragen rufen die entsprechende Funktionen in "bauteil".
	*/
	void setHerst(const string herst)	{ bauteil::setHerst(herst); }
	string getHerst()					{ return bauteil::getHerst(); }
	void setPreis(const int preis)		{ bauteil::setPreis(preis); }
	int getPreis()						{ return bauteil::getPreis(); }
	void setGang(const int gangsch)		{ _gangstellung = gangsch; }
	int getGang()						{ return _gangstellung; }

	void setGangMANUELL()				{ _typ = MANUELL; }
	void setGangAUTO()					{ _typ = AUTOMATISCH; }
	void setGangSEQ()					{ _typ = SEQUENTIELL; }
};

#endif	//	BAUTEIL_H
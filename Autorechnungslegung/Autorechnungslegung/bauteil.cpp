
#include "bauteil.h"


bauteil::bauteil(const bauteil *theO)
{
	_hersteller = theO->_hersteller;
	_preis = theO->_preis;
}

void bauteil::print() const
{
	cout << "\n\t\tHersteller:\t" << _hersteller << "\n\t\tPreis:\t\t" << _preis;
	return;
}


motor::motor(const motor *theO) : bauteil(theO)
{
	_pferdekraft = theO->_pferdekraft;
	_drehmoment	= theO->_drehmoment;
	_hubraum = theO->_hubraum;
	_kstoff = theO->_kstoff;
}

void motor::print() const
{
	cout << "\n\tMotor:";
	bauteil::print();
	cout << "\n\t\tPferdekraft:\t" << _pferdekraft << "\n\t\tDrehmoment:\t" <<
		_drehmoment << "\n\t\tHubraum:\t" << _hubraum;
	return;
}


gangschaltung::gangschaltung(const gangschaltung *theO) : bauteil(theO)
{
	_gangstellung = theO->_gangstellung;
	_typ = theO->_typ;
}

void gangschaltung::print() const
{
	cout << "\n\tGangschaltung:";
	bauteil::print();
	cout << "\n\t\tGangstellung:\t" << _gangstellung;
	return;
}
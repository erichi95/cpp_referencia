
#include <iostream>
#include "bauteil.h"
#include "auto.h"
#include "autohaendler.h"

using namespace std;

//!	Die Schnittstelle in "main" Funktion.
/*!
	Man soll die entsprechenden Daten hier eingeben f�r die Klassen:
		- "motor"
		- "gangschaltung"
		- "Auto"
		- "Autohaendler".
*/

int main()
{
	//!	Der Motor und die Gangschaltung sollen heterogenweise in einem Bauteil-array gesammelt werden.
	motor m("Mercedes", 300000, 200, 300, 1600, motor::BENZIN);
	gangschaltung g("NFS", 250000, 5, gangschaltung::MANUELL);
	bauteil *bt[Auto::MAX_BAUELEM];
	bt[0] = &m;
	bt[1] = &g;

	Auto a1("BMW", "m5", 5000000, bt);
	Auto a2("Mercedes", "clx", 10000000, bt);

	Autohaendler ah1("Papp", "Eger");

	ah1 += a1;
	ah1 += a2;

	ah1.print();



	Autohaendler ah2("Porsche", "Eger");

	ah2 += ah1;

	ah2.print();
	ah1.print();



	m.setHerst("BMW");
	m.setPreis(250000);
	m.setPferdk(300);
	m.setDreh(450);
	m.setHubraum(2000);
	m.setMotorDIESEL();
	
	g.setHerst("WAS");
	g.setPreis(300000);
	g.setGang(6);
	g.setGangAUTO();

	bt[0] = &m;
	bt[1] = &g;
	
	Auto a3("Volkswagen", "Golf 4", 3500000, bt);

	a1.setMarke("Audi");
	a1.setTyp("A4");
	a1.setPreis(7000000);

	ah1.setName("Etwas");
	ah1.setOrt("Budapest");

	ah1 += a1;
	ah1 += a3;

	ah1.print();



	cout << ah1.getOrt() << endl;


	getchar();
	return 0;
}
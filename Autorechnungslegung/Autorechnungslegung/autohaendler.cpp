
#include "autohaendler.h"


Autohaendler::~Autohaendler()
{
	if (_anz == 0) return;
	if (_anz == 1) {
		delete _sortiment[0];	_sortiment[0] = nullptr;
		delete _sortiment;		_sortiment = nullptr;
	}
	else {
		for (int i = 0; i < _anz; ++i) {
			delete _sortiment[i];	_sortiment[i] = nullptr;
		}
		delete[] _sortiment;	_sortiment = nullptr;
	}
}


void Autohaendler::print() const
{
	cout << "\nName: " << _name << "\nOrt: " << _ort << endl;

	if (_sortiment == nullptr) {
		cout << "Autohaendler ist leer!" << endl;
		cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
		return;
	}

	for (int i = 0; i < _anz; ++i) {
		if (_sortiment[i] != nullptr) _sortiment[i]->print();
	}
	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
	return;
}

void Autohaendler::operator+=(Auto &aut)
{
	++_anz;

	if (_anz == 1) {
		_sortiment = new Auto *;
		_sortiment[0] = new Auto(aut);
		return;
	}

	Auto **tmpsort = new Auto *[_anz];
	for (int i = 0; i < _anz - 1; ++i) {
		tmpsort[i] = new Auto(*_sortiment[i]);
		delete _sortiment[i];
		_sortiment[i] = nullptr;
	}
	tmpsort[_anz - 1] = new Auto(aut);

	if (_anz == 2)	delete _sortiment;
	else			delete[] _sortiment;
	_sortiment = tmpsort;

	return;
}

void Autohaendler::operator+=(Autohaendler &theO)
{
	if (theO._anz == 0) return;

	Auto **tmpsort = new Auto *[_anz + theO._anz];
	for (int i = 0; i < _anz; ++i) {
		tmpsort[i] = new Auto(*_sortiment[i]);
		delete _sortiment[i];
		_sortiment[i] = nullptr;
	}
	for (int i = 0; i < theO._anz; ++i) {
		tmpsort[i + _anz] = new Auto(*theO._sortiment[i]);
		delete theO._sortiment[i];
		theO._sortiment[i] = nullptr;
	}

	if (theO._anz == 1) delete theO._sortiment;
	else				delete[] theO._sortiment;
	theO._sortiment = nullptr;

	if (_anz == 1)		delete _sortiment;
	else				delete[] _sortiment;
	_sortiment = tmpsort;
	_anz += theO._anz;
	theO._anz = 0;

	return;
}
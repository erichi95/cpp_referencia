
#ifndef AUTO_H
#define AUTO_H

#include <iostream>
#include <string>

#include "bauteil.h"

using namespace std;

//! Klasse Auto.
/*!
	Diese Klasse entzh�lt die Marke, den Typ, den Preis, die Bauelemente eines Autos
	und die Konstruktoren, eine Destruktor, die print() und die getter(), setter() Funktionen.
*/
class Auto
{
private:
	string _marke;	//!< Die Marke des Autos (string).
	string _typ;	//!< Der Typ des Autos (string).
	int _preis;		//!< Der Preis des Autos (integer).

	bauteil **_bauelem;	/*!< Pointer-array von "bauteil".
						Maximale Anzahl der Elemente: 2 (sehe: MAX_BAUELEM).
						*/

public:
	static const int MAX_BAUELEM = 2;	//!< Maximale Anzahl der Bauelemente (static const integer).

	//! Konstruktor.
	/*!
		Die "bauteil" Elemente und Pointer-array werden kopiert,
		deshalb unabh�ngig von dem urspr�nglichen Pointer-array. 
	*/
	Auto(const string, const string, const int, bauteil**);

	//! Kopiekonstruktor, "bauteil" Elemente und Pointer-array werden kopiert.
	Auto(const Auto &);

	//! Destruktor.
	/*!
		Die "bauteil" Elemente und Pointer-array werden gel�scht,
		und auf Nullpointer gesetzt.
	*/
	~Auto();

	//! Die print() Funktion.
	/*!
		Schreibt die Daten des Autos auf dem Bildschirm aus.
		Ruft die print() Funktion die Bauelemente.
	*/
	void print() const;

	//! Die getter() und setter() Funktionen.
	void setMarke(const string marke)	{ _marke = marke; }
	string getMarke()					{ return _marke; }
	void setTyp(const string typ)		{ _typ = typ; }
	string getTyp()						{ return _typ; }
	void setPreis(const int preis)		{ _preis = preis; }
	int getPreis()						{ return _preis; }
};

#endif	//	AUTO_H